package eu.johanekdahl.Library;

import java.util.Scanner;

import static eu.johanekdahl.Library.StatsUtil.showAggregatedStats;
import static eu.johanekdahl.Library.StatsUtil.showHistoricTournaments;
import static eu.johanekdahl.Library.Utils.getRootMenuStrings;
import static eu.johanekdahl.Library.Utils.getStatsMenuStrings;
import static eu.johanekdahl.Play.normalPlay;

public class EndlessMenus {
	static Scanner scan = new Scanner(System.in);

	public static void getRootMenu() {
		outer:
		while (true){
			switch (inputHandler(getRootMenuStrings())) {
				case 1 -> normalPlay();
				case 2 -> getStatsMenu();
				case 3 -> {
					break outer;
				}
			}
		}

	}
	public static void getStatsMenu() {
		outer:
		while (true){
			switch (inputHandler(getStatsMenuStrings())) {
				case 1 -> showHistoricTournaments();
				case 2 -> showAggregatedStats();
				case 3 -> {
					break outer;
				}
			}

		}
	}

	public static int inputHandler(Runnable menuStrings) {
		int userInput = 0;
		do {
			menuStrings.run();
			if (scan.hasNextInt()) {
				userInput = scan.nextInt();
			} else {
				scan.nextLine();
			}
			if (userInput > 3 || userInput < 1){
				System.out.println("Enter valid value: 1, 2 or 3");
			}
		} while (userInput > 3 || userInput < 1);
		return userInput;
	}


}
