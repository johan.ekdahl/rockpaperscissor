package eu.johanekdahl.Library;

import java.util.Scanner;

public class Utils {
	public static int getResults(int a, int b) {
		if (a == b) return 1;
		else if ((((a - b) % 3) + 3) % 3 == 1) return 2;
		else return 0;
	}

	public static HandSigns getHandsign(int handsign) {
		return handsign == 0 ? HandSigns.ROCK : handsign == 1 ? HandSigns.PAPER : HandSigns.SCISSOR;
	}

	public static void printFormattedScore(int handsignUser, int handsignAI) {
		System.out.println("You chose " + getHandsign(handsignUser) + "\nAI chose " + getHandsign(handsignAI));
		switch (getResults(handsignUser, handsignAI)) {
			case 0 -> System.out.println("You lost");
			case 1 -> System.out.println("You tied");
			case 2 -> System.out.println("You won");
		}
		System.out.println("----------------------------------------------------");
	}

	public static String takeUserInputForPlayerName() {
		System.out.println("Enter your name");
		return new Scanner(System.in).nextLine();
	}

	public static Runnable getUserAlternativesStrings() {
		return () -> {
			System.out.println("----------------------------------------------------");
			System.out.println("1.Rock\n2.Paper\n3.Scissor");
		};
	}

	public static Runnable getStatsMenuStrings() {
		return () -> {
			System.out.println("----------------------------------------------------");
			System.out.println("1. Show all historic tournaments");
			System.out.println("2. Show your aggregated stats");
			System.out.println("3. Go to main menu");
		};
	}

	public static Runnable getRootMenuStrings() {
		return () -> {
			System.out.println("----------------------------------------------------");
			System.out.println("1. Play Tournament");
			System.out.println("2. Show Statistics");
			System.out.println("3. Quit");
		};
	}


}
