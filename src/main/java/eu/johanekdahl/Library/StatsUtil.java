package eu.johanekdahl.Library;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

public class StatsUtil {
	private static String PlayerName;
	static List<Timestamp> tsList = new ArrayList<>();
	static List<List<String>> tournamentResults = new ArrayList<>();
	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public static HashMap<String, Integer> playerPoints;



	public static void setPlayerName(String playerName) {
		PlayerName = playerName;
	}

	public static String getPlayerName() {
		return PlayerName;
	}

	public static void addTimeStamp() {
		tsList.add(new Timestamp(System.currentTimeMillis()));
		System.out.println("----------------------------------------------------");
		System.out.println(sdf.format(tsList.get(tsList.size() - 1)));
	}

	public static void scorePrintOut() {
		playerPoints.forEach((key, value) -> System.out.println(key + " " + value));
	}

	public static void showHistoricTournaments() {
		if(tournamentResults.size() != 0) {
			for (int i = 0; i < tournamentResults.size(); i++) {
				System.out.print(sdf.format(tsList.get(i)));
				for (int j = 0; j < tournamentResults.get(i).size(); j++) {
					System.out.print(" " + (j + 1) + ". " + tournamentResults.get(i).get(j));
				}
				System.out.println();
			}
		}
		else {
			System.out.println("No Stats Available");
		}
	}

	public static void saveTournamentStats() {
		List<String> sortedStats = playerPoints.entrySet().stream()
				.sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
				.map(Map.Entry::getKey).toList();
		tournamentResults.add(sortedStats);
	}


	public static void freshStatsMap() {
		playerPoints = new HashMap<>();
		playerPoints.put(getPlayerName(), 0);
		playerPoints.put("Slumpis", 0);
		playerPoints.put("Vokalis", 0);
		playerPoints.put("Klockis", 0);
	}

	public static void showAggregatedStats() {
		if(tournamentResults.size() != 0){
			int Best = Integer.MAX_VALUE;
			int Worst = Integer.MIN_VALUE;
			int Total = 0;

			for (List<String> tournamentResult : tournamentResults) {
				if (tournamentResult.indexOf(getPlayerName()) + 1 < Best) {
					Best = tournamentResult.indexOf(getPlayerName()) + 1;
				}
				if (tournamentResult.indexOf(getPlayerName()) + 1 > Worst) {
					Worst = tournamentResult.indexOf(getPlayerName()) + 1;
				}
				Total += tournamentResult.indexOf(getPlayerName()) + 1;
			}

			int Average = Math.round(Total / tournamentResults.size());
			System.out.println("Aggregated Stats:");
			System.out.println("Best: " + Best);
			System.out.println("Worst: " + Worst);
			System.out.println("Average: " + Average);
		}
		else {
			System.out.println("No Stats Available");
		}




	}
}
