package eu.johanekdahl.Library;

import java.util.Calendar;
import java.util.concurrent.ThreadLocalRandom;

public class StrategiesLibrary {
	public static int KlockisStrategy(){
		return Calendar.getInstance().get(Calendar.MINUTE)%3;
	}

	public static int SlumpisStrategy(){
		return ThreadLocalRandom.current().nextInt(0, 3);
	}

	public static int VokalisStrategy(){
		return StatsUtil.getPlayerName().replaceAll("[^aeiouAEIOU]","").length()%3;
	}
}
