package eu.johanekdahl;

import eu.johanekdahl.Decorators.addKlockis;
import eu.johanekdahl.Decorators.addSlumpis;
import eu.johanekdahl.Decorators.addVokalis;
import eu.johanekdahl.GameTypes.RockPaperScissor;
import eu.johanekdahl.Library.StatsUtil;

public class Play {
	public static void normalPlay(){
		StatsUtil.addTimeStamp();
		StatsUtil.freshStatsMap();
		//I used decorator pattern for this project, starting out with game RockPaperScissor, which is a child of base abstract class Game
		//I "decorate" this RockPaperScissor game with AI opponents dynamically, allowing for easy transition of tournament format.
		//in this stock format the User encounter every AI opponent once, while automatically the AIs encounter eachother once aswell.
		//
		new addKlockis(new addVokalis(new addSlumpis(new RockPaperScissor())));

		//I can easily extend the tournament format dynamically, which would allow us in the future to add for example an ingame menu with the options to add
		//whichever opponent you want.
//		Game rockpaperscissor2 = new addSlumpis(new addKlockis(new addVokalis(new addSlumpis(new RockPaperScissor()))));


		StatsUtil.scorePrintOut();
		StatsUtil.saveTournamentStats();
	}
}
