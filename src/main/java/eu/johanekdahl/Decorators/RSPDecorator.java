package eu.johanekdahl.Decorators;

import eu.johanekdahl.GameTypes.Game;

public abstract class RSPDecorator extends Game {

	public abstract void UserStrategy();
	public abstract void AIStrategy();
	public abstract void DisplayScore();
	public abstract void UpdateScore();
}
