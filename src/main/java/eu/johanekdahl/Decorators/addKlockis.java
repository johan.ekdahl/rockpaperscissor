package eu.johanekdahl.Decorators;

import eu.johanekdahl.GameTypes.Game;
import eu.johanekdahl.Library.StatsUtil;
import eu.johanekdahl.Library.StrategiesLibrary;

import static eu.johanekdahl.Library.EndlessMenus.inputHandler;
import static eu.johanekdahl.Library.Utils.*;
import static eu.johanekdahl.Library.Utils.getResults;

public class addKlockis extends RSPDecorator {
	Game game;
	//Rock:0 , Paper:1 , Scissor:2
	int handsignUser;
	int handsignAI;

	public addKlockis(Game game) {
		this.game = game;
		System.out.println("Your opponent is: Klockis");
		UserStrategy();
		AIStrategy();
		DisplayScore();
		UpdateScore();
	}

	@Override
	public void UserStrategy() {
		handsignUser = inputHandler(getUserAlternativesStrings());
	}

	@Override
	public void AIStrategy() {
		handsignAI = StrategiesLibrary.SlumpisStrategy();

	}

	@Override
	public void DisplayScore() {
		printFormattedScore(handsignUser,handsignAI);
	}

	@Override
	public void UpdateScore() {
		//Score 0-2
		//for User and this AI
		int User = getResults(handsignUser, handsignAI);
		StatsUtil.playerPoints.put(StatsUtil.getPlayerName(), StatsUtil.playerPoints.get(StatsUtil.getPlayerName()) + User);
		int AI = 2-User;
		StatsUtil.playerPoints.put("Klockis", StatsUtil.playerPoints.get("Klockis") + AI);


		//Autoplay between rest of the AIs
		int Slumpis = getResults(StrategiesLibrary.SlumpisStrategy(),StrategiesLibrary.VokalisStrategy());
		StatsUtil.playerPoints.put("Slumpis", StatsUtil.playerPoints.get("Slumpis")+Slumpis);
		int Vokalis = 2-Slumpis;
		StatsUtil.playerPoints.put("Vokalis", StatsUtil.playerPoints.get("Vokalis")+Vokalis);
	}
}
