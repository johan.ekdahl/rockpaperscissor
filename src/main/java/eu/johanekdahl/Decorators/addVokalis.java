package eu.johanekdahl.Decorators;

import eu.johanekdahl.GameTypes.Game;
import eu.johanekdahl.Library.StatsUtil;
import eu.johanekdahl.Library.StrategiesLibrary;

import static eu.johanekdahl.Library.EndlessMenus.inputHandler;
import static eu.johanekdahl.Library.Utils.*;
import static eu.johanekdahl.Library.Utils.getResults;

public class addVokalis extends RSPDecorator {
	Game game;
	//Rock:0 , Paper:1 , Scissor:2
	int handsignUser;
	int handsignAI;

	public addVokalis(Game game) {
		this.game = game;
		System.out.println("Your opponent is: Vokalis");
		UserStrategy();
		AIStrategy();
		DisplayScore();
		UpdateScore();
	}

	@Override
	public void UserStrategy() {
		handsignUser = inputHandler(getUserAlternativesStrings());
	}

	@Override
	public void AIStrategy() {
		handsignAI = StrategiesLibrary.SlumpisStrategy();

	}

	@Override
	public void DisplayScore() {
		printFormattedScore(handsignUser,handsignAI);
	}

	@Override
	public void UpdateScore() {
		//Score 0-2
		//for User and this AI
		int User = getResults(handsignUser, handsignAI);
		StatsUtil.playerPoints.put(StatsUtil.getPlayerName(), StatsUtil.playerPoints.get(StatsUtil.getPlayerName()) + User);
		int AI = 2-User;
		StatsUtil.playerPoints.put("Vokalis", StatsUtil.playerPoints.get("Vokalis") + AI);

		//Autoplay between rest of the AIs
		int Klockis = getResults(StrategiesLibrary.KlockisStrategy(),StrategiesLibrary.SlumpisStrategy());
		StatsUtil.playerPoints.put("Klockis", StatsUtil.playerPoints.get("Klockis")+Klockis);
		int Slumpis = 2-Klockis;
		StatsUtil.playerPoints.put("Klockis", StatsUtil.playerPoints.get("Klockis")+Slumpis);

	}
}
