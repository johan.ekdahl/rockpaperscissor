package eu.johanekdahl;

import eu.johanekdahl.Library.StatsUtil;

import static eu.johanekdahl.Library.EndlessMenus.getRootMenu;
import static eu.johanekdahl.Library.Utils.takeUserInputForPlayerName;

public class Main {
	public static void main(String[] args) {
			StatsUtil.setPlayerName(takeUserInputForPlayerName());
			getRootMenu();

	}
}
